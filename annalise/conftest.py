import pytest

from annalise.users.models import User
from annalise.images.models import Tag, Image
from annalise.images.tests.factories import TagFactory, ImageFactory
from annalise.users.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def tag() -> Tag:
    return TagFactory()


@pytest.fixture
def image() -> Image:
    return ImageFactory()
