from rest_framework import serializers
from .models import Tag, Image


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class ImageSerializer(serializers.ModelSerializer):
    display_tags = serializers.StringRelatedField(source='tags', many=True, read_only=True)
    last_updated = serializers.DateTimeField(source='modified', read_only=True)

    class Meta:
        model = Image
        fields = ('id', 'file', 'created', 'last_updated', 'display_tags', 'tags')
        extra_kwargs = {'tags': {'write_only': True}}
