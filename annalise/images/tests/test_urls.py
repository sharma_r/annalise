import pytest
from django.urls import resolve, reverse

from annalise.images.models import Tag, Image

pytestmark = pytest.mark.django_db

#########################
# Tests for tags URLs
#########################

def test_tags_detail(tag: Tag):
    assert (
        reverse("api:tag-detail", kwargs={"pk": tag.id})
        == f"/api/tags/{tag.id}/"
    )
    assert resolve(f"/api/tags/{tag.id}/").view_name == "api:tag-detail"


def test_tags_list():
    assert reverse("api:tag-list") == "/api/tags/"
    assert resolve("/api/tags/").view_name == "api:tag-list"

#########################
# Tests for images URLs
#########################


def test_images_detail(image: Image):
    assert (
        reverse("api:image-detail", kwargs={"pk": image.id})
        == f"/api/images/{image.id}/"
    )
    assert resolve(f"/api/images/{image.id}/").view_name == "api:image-detail"


def test_images_list():
    assert reverse("api:image-list") == "/api/images/"
    assert resolve("/api/images/").view_name == "api:image-list"
