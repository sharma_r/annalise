import pytest
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from django.urls import reverse
from rest_framework.test import force_authenticate

from annalise.images.tests.factories import ImageFactory
from annalise.images.views import TagViewSet, ImageViewSet
from annalise.images.models import Tag, Image
from annalise.users.models import User
from PIL import Image as PILImage
import json
from io import BytesIO

pytestmark = pytest.mark.django_db


class TestTagViewSet:

    """
    Test CRUD operations for the tags endpoint
    """

    def test_list(self, user: User, tag: Tag, rf: RequestFactory):
        url = reverse("api:tag-list")
        request = rf.get(url)
        force_authenticate(request, user=user)
        view = TagViewSet()

        view.request = request

        assert tag in view.get_queryset()

    def test_retrieve(self, user: User, tag: Tag, rf: RequestFactory):
        expected_json = {
            "id": str(tag.id),
            "name": tag.name
        }
        url = reverse("api:tag-detail", kwargs={"pk": tag.id})
        request = rf.get(url)
        view = TagViewSet.as_view({'get': 'retrieve'})
        force_authenticate(request, user=user)

        response = view(request, pk=tag.id).render()
        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    def test_create(self, user: User, rf: RequestFactory):
        valid_data_dict = {
            "name": "test"
        }
        url = reverse('api:tag-list')
        request = rf.post(
            url,
            content_type='application/json',
            data=json.dumps(valid_data_dict)
        )
        force_authenticate(request, user=user)
        view = TagViewSet.as_view(
            {'post': 'create'}
        )

        response = view(request).render()

        assert response.status_code == 201

    def test_delete(self, user: User, tag: Tag, rf: RequestFactory):
        url = reverse('api:tag-detail', kwargs={'pk': tag.id})
        request = rf.delete(url)
        force_authenticate(request, user=user)
        view = TagViewSet.as_view(
            {'delete': 'destroy'}
        )

        response = view(request, pk=tag.id).render()

        assert response.status_code == 204

    def test_partial_update(self, user: User, tag: Tag, rf: RequestFactory):
        url = reverse('api:tag-detail', kwargs={'pk': tag.id})
        valid_data_dict = {
            "name": "test"
        }
        request = rf.patch(url,
                       content_type='application/json',
                       data=json.dumps(valid_data_dict)
                   )
        force_authenticate(request, user=user)
        view = TagViewSet.as_view(
            {'patch': 'partial_update'}
        )

        response = view(request, pk=tag.id).render()

        assert response.status_code == 200
        assert json.loads(response.content)['name'] == valid_data_dict['name']


class TestImageViewSet:

    """
    Test CRUD operations for the images endpoint
    Also includes test for searching
    """

    def get_image_file(self, name="test.jpeg", ext="jpeg", size=(50, 50), color=(256, 0, 0)):
        """
        A dummy image is generated for in memory for testing the create operation
        """
        file_obj = BytesIO()
        image = PILImage.new("RGB", size=size, color=color)
        image.save(file_obj, ext)
        return SimpleUploadedFile(name, file_obj.getvalue())

    def test_list(self, user: User, image: Image, rf: RequestFactory):
        url = reverse("api:image-list")
        request = rf.get(url)
        force_authenticate(request, user=user)
        view = ImageViewSet()

        view.request = request

        assert image in view.get_queryset()

    def test_retrieve(self, user: User, image: Image, rf: RequestFactory):
        url = reverse("api:image-detail", kwargs={"pk": image.id})
        request = rf.get(url)
        view = ImageViewSet.as_view({'get': 'retrieve'})
        force_authenticate(request, user=user)

        response = view(request, pk=image.id).render()
        assert response.status_code == 200
        assert json.loads(response.content)['file'] == request.build_absolute_uri(image.file.url)

    def test_create(self, user: User, tag: Tag, rf: RequestFactory):
        valid_data_dict = {
            "file": self.get_image_file(),
            "tags[]": str(tag.id),
        }
        url = reverse('api:image-list')
        request = rf.post(
            url,
            format='multipart',
            data=valid_data_dict
        )
        force_authenticate(request, user=user)
        view = ImageViewSet.as_view(
            {'post': 'create'}
        )

        response = view(request).render()

        assert response.status_code == 201

    def test_delete(self, user: User, image: Image, rf: RequestFactory):
        url = reverse('api:image-detail', kwargs={'pk': image.id})
        request = rf.delete(url)
        force_authenticate(request, user=user)
        view = ImageViewSet.as_view(
            {'delete': 'destroy'}
        )

        response = view(request, pk=image.id).render()

        assert response.status_code == 204

    def test_partial_update(self, user: User, tag: Tag, image: Image, rf: RequestFactory):
        url = reverse('api:image-detail', kwargs={'pk': image.id})
        valid_data_dict = {
            "tags[]": str(tag.id)
        }
        request = rf.patch(url,
                           content_type='application/json',
                           data=json.dumps(valid_data_dict))
        force_authenticate(request, user=user)
        view = ImageViewSet.as_view(
            {'patch': 'partial_update'}
        )

        response = view(request, pk=image.id).render()

        assert response.status_code == 200

    def test_search(self, user: User, tag: Tag, rf: RequestFactory):
        image = ImageFactory()
        image.tags.set([tag])
        url = f"{reverse('api:tag-list')}?search={tag.name}"
        request = rf.get(url)
        force_authenticate(request, user=user)

        view = ImageViewSet.as_view({'get': 'list'})
        response = view(request).render()
        assert response.status_code == 200
        assert json.loads(response.content)[0]['file'] == request.build_absolute_uri(image.file.url)
