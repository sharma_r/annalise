import factory
from factory import Faker
from factory.django import DjangoModelFactory
from ..models import Tag, Image


class TagFactory(DjangoModelFactory):
    """
    Test factory to generate Tag objects for testing
    """

    name = Faker("name")

    class Meta:
        model = Tag


class ImageFactory(DjangoModelFactory):
    """
    Image factory to generate Image objects for testing
    """

    file = factory.django.ImageField(width=1024, height=768)

    class Meta:
        model = Image

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of tags were passed in, use them
            for tag in extracted:
                self.tags.add(tag)
