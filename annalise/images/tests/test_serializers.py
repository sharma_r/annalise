import pytest

from annalise.images.serializers import TagSerializer, ImageSerializer
from annalise.images.models import Tag, Image

pytestmark = pytest.mark.django_db


class TestTagSerializer:
    """
    Test the serializer for Tag objects
    """

    def test_serialize_model(self, tag: Tag):
        serializer = TagSerializer(tag)
        assert serializer.data

    def test_serialize_data(self, tag: Tag):
        expected_serialized_data = {
            'id': tag.id,
            'name': tag.name,
        }
        serializer = TagSerializer(data=expected_serialized_data)
        assert serializer.is_valid()
        assert serializer.errors == {}


class TestImageSerializer:
    """
    Test the serializer for Image objects
    """

    def test_serialize_model(self, image: Image):
        serializer = ImageSerializer(image)
        assert serializer.data

    def test_serialize_data(self, image: Image):
        expected_serialized_data = {
            'id': image.id,
            'file': image.file,
            'display_tags': image.tags,
            'created': image.created,
            'last_updated': image.modified,
        }
        serializer = ImageSerializer(data=expected_serialized_data)
        assert serializer.is_valid()
        assert serializer.errors == {}
