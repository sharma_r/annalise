from django.db import models
import uuid


class Tag(models.Model):
    """
    Tags to be associated with images. Reason for creating a separate schema for tags is to allow uniformity across
    all the tagged images (free text could have spelling errors etc that would make labelling ineffective), plus the
    ease with which tag name could be changed across all the associated images
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class Image(models.Model):
    """
    The assumption here is that since multiple images could have same label/tag (and vice-versa), it would make more
    sense to have many to many relationship between them to allow that flexibility (works for 1-to-1 cases too)
    """
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    file = models.ImageField(upload_to="uploads")
    tags = models.ManyToManyField(Tag, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
