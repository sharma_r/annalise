from rest_framework import viewsets, filters
from rest_framework_tracking.mixins import LoggingMixin

from .models import Tag, Image
from .serializers import TagSerializer, ImageSerializer


class TagViewSet(LoggingMixin, viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class ImageViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    This gets the list of images, ordered by most recently updated/created. Also has the feature to search by images
    """
    queryset = Image.objects.all().order_by("-modified", "-created")
    serializer_class = ImageSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['tags__name']
