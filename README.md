annalise
========

Solution to images and tags assignment

Acknowledgements: This project has been bootstrapped with Django cookiecutter for a production-grade project and to make
development process as quick as possible (https://github.com/pydanny/cookiecutter-django/)

Architecture overview
---------------------

* Tags and Images are both persisted in database. Tags have been defined as separate objects that have many-to-many 
relationship with the images. Reason for creating a separate schema for tags is to allow uniformity across all the 
tagged images (free text could have spelling errors etc that would make labelling ineffective), plus the ease with which tag name could be changed across all the associated images.
The database schema for both can be seen in **annalise/images/models.py** file.

* There are endpoints for managing both tags and images (more details in the **endpoints** section below)

* Unauthorized access to these endpoints is prohibited. User needs to be logged in to perform
operations on both endpoints. The access level to images & tags for all users is same for sake of simplicity.
However, user actions are tracked through logs, which can be accessed from Django's admin panel (http://localhost:9000/admin
in development environment). Once logged into the admin panel, user logs can be seen in API request logs section

* The config for whole project has been split into two parts - one for local development and one for
production environment. Most of these config settings came with the cookiecutter project, but that's generally how I prefer to
organise my projects anyway, which is primarily the reason I chose it as base. The environment variables are stored
in .envs/ folder, Django settings in config/ folder, and the main application resides in annalise/ folder.

In summary, following is the project structure:
* .envs/ - stores environment variables for local and prod
* .annalise/ - stores the main application code
    * images/ - contains the code for images and tags schema & endpoints
    * users/ - contains the code for users
    * media/ - images are stored here in local/test environments (gitignored)
* .compose/ - stores the docker configuration files for local and prod environments
* config/ - stores Django settings, urls, and wsgi files
* requirements/ - stores the Python packages required by project in different environments
* local.yml - Docker-compose file for local/test environment
* production.yml - Docker-compose file for prod environment


Getting started
---------------

The best way to run this project would be with the following docker-compose command from the same directory where local.yml
is located

    docker-compose -f local.yml up -d --build

The local.yml file contains the config suitable for running this project
on a testing environment. production.yml contains the settings for production environment (these configs
came with Cookiecutter project itself)

Once it is built, you need to run the migrations from same folder to apply the database changes with the following command:

    docker-compose -f local.yml run django python manage.py migrate

These actions are necessary for the first time. However, second time onwards the docker containers can be run simply
with 

    docker-compose -f local.yml up

Basic Commands
--------------

Setting Up Users

Since apis cannot be accessed without authorized access, you need to have user account for the same.
The current setup considers all users equally privileged and allows anyone with an account to be able to read, add, update,
and delete tags and images. For simplicity's sake, permissions are not enforced in this assignment.

* To create an **superuser account**, use this command

        $ docker-compose -f local.yml python manage.py createsuperuser

    Once you have added a superuser, you can go to http://localhost:9000/admin, log in with
    your created credentials and create rest of the users from the admin panel too

Getting an auth token

* To work with the api endpoints in this project, you need to get authentication token for your user
by querying the http://localhost:9000/auth-token with username/password combination

Alternative to querying via CLI:

* If you prefer to make queries through a GUI interface rather than through command line, you can log in
via the Django admin panel (http://localhost:9000/admin) and then visit http://localhost:9000/api to get access
to the available endpoints for rest of the operations.

To run the tests in the project, you can use the following command

    $ docker-compose -f local.yml run django pytest

Once you have finished testing the project, you can shut it down with

    $ docker-compose -f local.yml down

Endpoints
---------

* Images

        http://localhost:9000/api/images/

    This is the **base url** of the endpoint for images, and supports the following operations:
    
    * **List** - you can get all the images with GET request to the base url, shown by most recently modified/created entry first
    * **Retrieve** - you can get specific image and its details with a GET request by adding the UUID of the image at the end to the base url, like http://localhost:9000/api/images/3f87585c-b9be-4736-9f6a-aa66f4e63280/
    * **Create** - you can upload a new image (with or without tags) through a POST request to the base url. All the images will be stored in the media/uploads folder
        * file - you need to pass the image with this field
        * tags - this is optional but you can also pass UUID of tags at time image is recorded
    * **Delete** - you can delete selected image with a DELETE request and adding UUID of the image you want to delete to the base URL
    * **Update** - you can add/update tags (or other editable information) for an image through partial update via PATCH request
    * **Search** - you can search images through tags with query parameter in the base request, like http://localhost:9000/api/images?search=test
    
* Tags

        http://localhost:9000/api/tags/

    This is the **base url** of the endpoint for images, and supports the following operations:
    
    * **List** - you can get all the tags with GET request to the base url, shown by most recently modified/created entry first
    * **Retrieve** - you can get specific tag with a GET request by adding the UUID of the tag at the end to the base url, like http://localhost:9000/api/tags/3f87585c-b9be-4736-9f6a-aa66f4e63280/
    * **Create** - you can upload a new tag through a POST request to the base url
        * name - this is the only parameter you need to pass to the creation endpoint
    * **Delete** - you can delete selected tag with a DELETE request and adding UUID of the tag you want to delete to the base URL
    * **Update** - you can update a tag via the PATCH/PUT request and including the UUID of the tag to the base URL


Deployment
----------

As mentioned in the architecture overview, the project settings have been split into two main environments - local and production.
The commands described above have been using the local environment settings. Similarly, for production, production config can be used.
**production.yml** can be used for docker-compose

For production, instead of Django's inbuilt server (which is not secure and not recommended) for testing, gunicorn will be used.

For CI/CD a Travis file has been included, and some of the settings in the production settings have been set for AWS (like S3 storage
for media/static files). However, because of the isolation of the environments, it should be relatively simple to extend the configuration
to meet any other infrastructure needs.
