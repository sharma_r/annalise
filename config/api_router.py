from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from annalise.users.views import UserViewSet
from annalise.images.views import TagViewSet, ImageViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("tags", TagViewSet)
router.register("images", ImageViewSet)


app_name = "api"
urlpatterns = router.urls
